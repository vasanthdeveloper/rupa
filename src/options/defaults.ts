/*
 *  Stores default options for rupa formatter.
 *  Created On 13 July 2021
 */

export default {
    indent: {
        content: 1,
        subContent: 3,
    },
    colors: {
        arg: ['yellowBright'],
        command: ['blueBright'],
        heading: ['bold', 'whiteBright'],
    },
}
